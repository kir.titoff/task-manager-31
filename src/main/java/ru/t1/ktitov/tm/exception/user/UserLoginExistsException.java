package ru.t1.ktitov.tm.exception.user;

public final class UserLoginExistsException extends AbstractUserException {

    public UserLoginExistsException() {
        super("Error! User with such login exists.");
    }

}
